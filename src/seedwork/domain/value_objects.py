import uuid

UUID = uuid.UUID
UUID.v4 = uuid.uuid4


def generate_uuid():
    return str(uuid.uuid4())
