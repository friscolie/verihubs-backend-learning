from sqlalchemy import Column, Integer, String, ForeignKey, DATETIME
from ....seedwork.infra.database import Base

from ....seedwork.domain.value_objects import generate_uuid


class Users(Base):
    __tablename__ = "users"

    id = Column(String(36),
                primary_key=True, default=generate_uuid)
    username = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    role = Column(String)
    deleted_at = Column(DATETIME, nullable=True)
