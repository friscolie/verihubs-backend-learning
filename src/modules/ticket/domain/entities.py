from sqlalchemy import Column, Integer, String, Enum
from ....seedwork.infra.database import Base
from ....seedwork.domain.value_objects import generate_uuid


class Tickets(Base):
    __tablename__ = "tickets"

    id = Column(String(36), primary_key=True, default=generate_uuid)
    title = Column(String)
    problem = Column(String)
    product_name = Column(String)
    product_version = Column(String)
    cs_id = Column(String(36), nullable=False)
    creator_id = Column(String(36), nullable=False)
    status = Column(Enum("open", "close"), default="open")
