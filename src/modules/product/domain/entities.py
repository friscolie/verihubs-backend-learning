from sqlalchemy import DATETIME, Column, String
from ....seedwork.infra.database import Base
import uuid
from ....seedwork.domain.value_objects import generate_uuid


class Products(Base):
    __tablename__ = "products"

    id = Column(String(36),
                primary_key=True, default=generate_uuid)
    product_name = Column(String, unique=True)
    product_version = Column(String, unique=True)
    deleted_at = Column(DATETIME, nullable=True)
